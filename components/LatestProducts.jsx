import React from 'react';
import Center from './Center';
import styled from 'styled-components';
import ProductCard from './ProductCard';

const ProductGrid = styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr 1fr;
    gap : 50px ;
    padding-top: 20px;
`;



export default function LatestProducts({ newProduct }) {
  return (
    <>
      <Center>
        <h1>Newest Arrivals</h1>
        <ProductGrid>
          {newProduct?.map((p) => (
            <>
            <ProductCard {...p}/>
            </>
            ))}

        </ProductGrid>
      </Center>
    </>
  );
}
