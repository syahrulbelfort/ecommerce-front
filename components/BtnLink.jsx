import React from "react";
import styled from "styled-components";
import Link from "next/link";
import { BtnStyle } from "./PrimaryBtn";

const StyledLink = styled(Link)`
  ${BtnStyle}
`;

export default function BtnLink(props) {
  return <StyledLink {...props} />;
}
