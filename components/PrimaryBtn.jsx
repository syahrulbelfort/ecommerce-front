import React from "react";
import styled, { css } from "styled-components";

export const BtnStyle = css`
  background-color: black;
  color: white;
  padding: 8px;
  border-radius: 4px;
  border: 0;
  cursor: pointer;
  margin-top: 0px;
  text-decoration: none;

  ${(props) =>
    props.size === "l" &&
    css`
      font-size: 1.2rem;
      padding: 10px 20px;
    `}

  ${(props) =>
    props.$transparent &&
    css`
      background-color: white;
      border: 1px solid black;
      color: black;
    `}
    ${(props) =>
    props.$block &&
    css`
      display: block;
      width: 100%;
    `}
`;

const StyledBtn = styled.button`
  ${BtnStyle}
`;

export default function PrimaryBtn({ children, ...rest }) {
  return <StyledBtn {...rest}>{children}</StyledBtn>;
}
