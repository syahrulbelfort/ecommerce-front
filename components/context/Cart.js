import React, { createContext, useState, useEffect } from 'react';

export const CartContext = createContext({});

function CartContextProvider({ children }) {
const [cartProducts, setCartProducts] = useState([]);
const ls = typeof window !== 'undefined' ? window.localStorage : null;

useEffect(()=>{
    if(cartProducts.length > 0){
        ls.setItem('cart', JSON.stringify(cartProducts))
    }
},[cartProducts]);

useEffect(()=>{
    if( ls && ls.getItem('cart')){
        setCartProducts(JSON.parse(ls.getItem('cart')))
    } 
},[])

const addToBag = (productId) => {
setCartProducts((prev) => [...prev, productId]);
};

const reduceBag = (productId) =>{
    setCartProducts((prev)=>{
        const position = prev.indexOf(productId)
        if(position !== -1){
           return prev.filter((value, index)=> index !== position)
        }
        return prev
    })
}

function clearCart() {
    ls.removeItem('cart');
    setCartProducts([]);
  }
  

return (
<CartContext.Provider value={{ cartProducts, setCartProducts, addToBag, reduceBag, clearCart }}>
{children}
</CartContext.Provider>
);
}

export default CartContextProvider;