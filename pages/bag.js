/* eslint-disable react-hooks/rules-of-hooks */

import Center from "@/components/Center";
import Input from "@/components/Input";
import PrimaryBtn from "@/components/PrimaryBtn";
import Table from "@/components/Table";
import { CartContext } from "@/components/context/Cart";
import axios from "axios";
import { useContext, useEffect, useState } from "react";
import styled from "styled-components";

const Box = styled.div`
  box-shadow: rgba(99, 99, 99, 0.2) 0px 2px 8px 0px;
  border-radius: 10px;
  padding: 40px;
`;

const ColumnWrapper = styled.div`
  display: grid;
  grid-template-columns: 1.3fr 0.7fr;
  gap: 40px;
  margin-top: 100px;
`;

const ProductBox = styled.div`
  box-shadow: rgba(99, 99, 99, 0.2) 0px 2px 8px 0px;
  border-radius: 10px;
  width: 100px;
  height: 100px;
  padding: 20px;
  margin-top: 10px;
  display: flex;
  align-items: center;
  justify-content: center;
  img {
    max-width: 100px;
    max-height: 100px;
  }
`;

const QtyProduct = styled.span`
  padding: 0 5px;
`;

const CityHolder = styled.div`
  display: flex;
  gap: 5px;
`;

const ProductInfo = styled.td`
  display: flex;
  align-items: start;
  flex-direction: column;
`;

const TotalAmount = styled.td`
  padding-top: 20px;
  font-weight: bold;
`;

export default function bag() {
  const { cartProducts, addToBag, reduceBag, clearCart } =
    useContext(CartContext);
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [city, setCity] = useState("");
  const [postalCode, setPostalCode] = useState("");
  const [streetAddress, setStreetAddress] = useState("");
  const [country, setCountry] = useState("");
  const [products, setProducts] = useState([]);
  const [isSuccess, setIsSuccess] = useState(false);

  useEffect(() => {
    if (cartProducts.length > 0) {
      axios.post("/api/bag", { ids: cartProducts }).then((response) => {
        setProducts(response.data);
      });
    } else {
      localStorage.removeItem("cart");
      setProducts([]);
    }
  }, [cartProducts]);

  useEffect(() => {
    if (typeof window === "undefined") {
      return;
    }
    if (window?.location.href.includes("success")) {
      setIsSuccess(true);
      clearCart();
    }
  }, []);

  function addMore(id) {
    addToBag(id);
  }
  function minusMore(id) {
    reduceBag(id);
  }

  let total = 0;

  for (const productId of cartProducts) {
    const price = products.find((p) => p._id === productId)?.price || 0;
    total += price;
  }

  if (isSuccess) {
    return (
      <>
        <Center>
          <Box>
            <h1>Thanks for your order!</h1>
            <p>We will email you when your order is sent.</p>
          </Box>
        </Center>
      </>
    );
  }

  async function goToPayment() {
    const response = await axios.post("/api/checkout", {
      name,
      email,
      city,
      postalCode,
      streetAddress,
      country,
      cartProducts,
    });
    if (response.data.url) {
      window.location = response.data.url;
    }
  }

  return (
    <div>
      <Center>
        <ColumnWrapper>
          <Box>
            <h2>Bag</h2>
            {!cartProducts.length && <h2>Your bag is empty</h2>}
            {products?.length > 0 && (
              <Table>
                <thead>
                  <tr>
                    <th>Product</th>
                    <th>Quantity</th>
                    <th>Price</th>
                  </tr>
                </thead>
                <tbody>
                  {products.map((product) => (
                    <tr key={product._id}>
                      <ProductInfo>
                        <ProductBox>
                          <img src={product.images[0]} alt="" />
                        </ProductBox>
                        <h3>{product.title}</h3>
                      </ProductInfo>
                      <td>
                        <button onClick={() => minusMore(product._id)}>
                          -
                        </button>
                        <QtyProduct>
                          {
                            cartProducts.filter((id) => id === product._id)
                              .length
                          }
                        </QtyProduct>
                        <button onClick={() => addMore(product._id)}>+</button>
                      </td>
                      <td>
                        IDR{" "}
                        {(
                          cartProducts.filter((id) => id === product._id)
                            .length * product.price
                        ).toLocaleString()}
                      </td>
                    </tr>
                  ))}
                  <tr>
                    <td>
                      <h3>Total amount</h3>
                    </td>
                    <td></td>
                    <td>
                      <h3>IDR {total.toLocaleString()}</h3>
                    </td>
                  </tr>
                </tbody>
              </Table>
            )}
          </Box>
          <Box>
            <h2>Order information</h2>
            <Input
              type="text"
              placeholder="Name"
              value={name}
              name="name"
              onChange={(e) => setName(e.target.value)}
            />
            <Input
              type="text"
              placeholder="Email"
              value={email}
              name="email"
              onChange={(e) => setEmail(e.target.value)}
            />
            <CityHolder>
              <Input
                type="text"
                placeholder="City"
                value={city}
                name="city"
                onChange={(e) => setCity(e.target.value)}
              />
              <Input
                type="text"
                placeholder="Zip code"
                value={postalCode}
                name="postalCode"
                onChange={(e) => setPostalCode(e.target.value)}
              />
            </CityHolder>
            <Input
              type="text"
              placeholder="Address"
              value={streetAddress}
              onChange={(e) => setStreetAddress(e.target.value)}
              name="streetAddress"
            />
            <Input
              type="text"
              placeholder="Country"
              value={country}
              name="country"
              onChange={(e) => setCountry(e.target.value)}
            />
            <PrimaryBtn onClick={goToPayment} $block>
              Continue to payment
            </PrimaryBtn>
          </Box>
        </ColumnWrapper>
      </Center>
    </div>
  );
}
