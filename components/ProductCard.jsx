import  styled from "styled-components";
import Link from "next/link";
import { useContext } from "react";
import { CartContext } from "./context/Cart";
import PrimaryBtn from "./PrimaryBtn";



const ProductWrapper = styled.div`
    
`

const Box = styled(Link)`
box-shadow: rgba(99, 99, 99, 0.2) 0px 2px 8px 0px;
padding: 50px;
text-align: center;
display: flex;
align-items: center;
border-radius: 10px;
height: 120px;
margin-bottom: 10px;
img{
    max-width: 100%;
    max-height: 100%;
}
`

const Title = styled(Link)`
    font-weight: normal;
    font-size: 1rem;
    text-decoration: none;
    color: inherit;
`

const ProductInfo = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-between;
    margin: 15px 0;
`

const Price = styled.div`
    font-size: 1.5rem;
    font-weight: bold;
`



export default function ProductCard({_id, title,price, images}) {
    const URL = `/product/${_id}`

    const {addToBag} = useContext(CartContext)
    const addProduct = () =>{
        addToBag(_id)
    }
  return (
    <ProductWrapper>
        <Box href={URL}>
            <img src={images[0]}/>
        </Box>
            <Title href={URL}>{title}</Title>
            <ProductInfo>
                <Price>
                    IDR {price.toLocaleString()}
                </Price>
            <PrimaryBtn onClick={addProduct}>add to bag</PrimaryBtn>
            </ProductInfo>
    </ProductWrapper>
  )
}
