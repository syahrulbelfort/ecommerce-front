import Featured from "@/components/Featured";
import Header from "@/components/Nav/Navbar";
import LatestProducts from "@/components/LatestProducts";
import { MongooseConnect } from "@/libs/mongoose";
import { Product } from "@/models/products";
import Head from "next/head";

export default function Home({ featuredProduct, newProduct }) {
  return (
    <>
      <Head>
        <title>Nike store</title>
        <link
          rel="icon"
          href="https://www.seekpng.com/png/full/1-15578_nike-logo-png-nike-logo-white-png.png"
        />
      </Head>
      <Featured product={featuredProduct} />
      <LatestProducts newProduct={newProduct} />
    </>
  );
}

export async function getServerSideProps() {
  const featuredProductId = "649812b6526b5cb3c57fdf48";
  await MongooseConnect();
  const featuredProduct = await Product.findById(featuredProductId);
  const newProducts = await Product.find().sort({ _id: -1 }).limit(10);
  return {
    props: {
      featuredProduct: JSON.parse(JSON.stringify(featuredProduct)),
      newProduct: JSON.parse(JSON.stringify(newProducts)),
    },
  };
}
