import React from 'react'
import  styled from 'styled-components'

const StyledTable = styled.table`
    width : 100%;
    th{
        text-align: left;
        text-transform: uppercase;
        color: #aaa;
        font-weight: bold;
        font-size: 1rem;
    }
    td{
        border-top: 1px solid #aaa;
    }
    h4{
        font-weight: normal;
    }
`

export default function Table(props) {
  return (
    <StyledTable {...props}/>
    )
}
