import { MongooseConnect } from "@/libs/mongoose";
import { Order } from "@/models/order";
import { buffer } from "micro";
const stripe = require("stripe")(process.env.STRIPE_SK);
const endpointSecret =
  "whsec_0692917b32e58cefcaed6592c49d2b139388fa6236ef60d61818697ba06e08b5";

export default async function handler(req, res) {
  await MongooseConnect();
  const sig = req.headers["stripe-signature"];

  let event;

  try {
    event = stripe.webhooks.constructEvent(
      await buffer(req),
      sig,
      endpointSecret
    );
  } catch (err) {
    res.status(400).send(`Webhook Error: ${err.message}`);
    return;
  }

  // Handle the event
  switch (event.type) {
    case "checkout.session.completed":
      const data = event.data.object;
      console.log(data);
      const orderId = data.metadata.orderId;
      const paid = data.payment_status === "paid";
      if (orderId && paid) {
        await Order.findByIdAndUpdate(orderId, {
          paid: true,
        });
      }
      break;
    default:
      console.log(`Unhandled event type ${event.type}`);
  }

  res.status(200).send("ok");
}

export const config = {
  api: { bodyParser: false },
};

//fairly-prompt-yay-frugal
