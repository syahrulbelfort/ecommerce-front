import { MongooseConnect } from '@/libs/mongoose'
import { Product } from '@/models/products';

export default async function handler(req,res) {
  await MongooseConnect()  
  const ids = req.body.ids;
  res.json(await Product.find({_id:ids}));
}
