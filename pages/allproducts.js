import Center from "@/components/Center";
import { MongooseConnect } from "@/libs/mongoose";
import { Product } from "@/models/products";
import ProductsGrid from "@/components/ProductsGrid";

export default function ProductsPage({ products }) {
  return (
    <>
      <Center>
        <h1>All products</h1>
        <ProductsGrid products={products} />
      </Center>
    </>
  );
}

export async function getServerSideProps() {
  await MongooseConnect();
  const products = await Product.find({}, null, { sort: { _id: -1 } });
  return {
    props: {
      products: JSON.parse(JSON.stringify(products)),
    },
  };
}
