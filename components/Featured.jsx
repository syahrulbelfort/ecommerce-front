import React, { useContext } from "react";
import Center from "./Center";
import styled from "styled-components";
import BtnLink from "./BtnLink";
import PrimaryBtn from "./PrimaryBtn";
import { CartContext } from "./context/Cart";

const Title = styled.h1`
  font-weight: bold;
  margin: 0;
  font-size: 2.2rem;
`;

const ColumnsWrapper = styled.div`
  display: flex;
  flex-direction: column-reverse;
  gap: 20px;
  img {
    max-width: 350px;
  }
  @media screen and (min-width: 768px) {
    display: grid;
    grid-template-columns: 1fr 1fr;
    img {
      max-width: 500px;
    }
  }
`;

const BtnWrapper = styled.div`
  display: flex;
  gap: 4px;
`;

const BG = styled.div`
  padding: 100px 0;
`;

const Column = styled.div`
  display: flex;
  align-items: center;
`;

export default function Featured({ product }) {
  const { addToBag } = useContext(CartContext);

  const addProduct = () => {
    addToBag(product._id);
  };

  return (
    <>
      <BG>
        <Center>
          <ColumnsWrapper>
            <Column>
              <div>
                <Title>{product.title}</Title>
                <p>{product.description}</p>
                <BtnWrapper>
                  <BtnLink
                    href={`/product/${product._id}`}
                    size="l"
                    $transparent>
                    Read more
                  </BtnLink>
                  <PrimaryBtn onClick={addProduct} size="l">
                    Add to bag
                  </PrimaryBtn>
                </BtnWrapper>
              </div>
            </Column>
            <Column>
              <img src={product.images[0]} alt={product.title} />
            </Column>
          </ColumnsWrapper>
        </Center>
      </BG>
    </>
  );
}
