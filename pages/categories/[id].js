/* eslint-disable react/jsx-key */
import Center from "@/components/Center";
import Loader from "@/components/Loader";
import ProductsGrid from "@/components/ProductsGrid";
import { Category } from "@/models/categories";
import { Product } from "@/models/products";
import axios from "axios";
import { useEffect, useState } from "react";
import styled from "styled-components";

const CatHeader = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const FilterWrap = styled.div`
  display: flex;
  gap: 15px;
  align-items: center;
`;

const Filter = styled.div`
  background-color: #ddd;
  padding: 5px 20px;
  font-size: 1.2em;
  text-align: start;
  gap: 10px;
  display: flex;
  border-radius: 5px;
  select {
    background-color: transparent;
    border: none;
    font-size: inherit;
  }
`;

export default function SingleCategories({
  category,
  subCategories,
  products: defaultProduct,
}) {
  const defaultSorting = "_id-desc";
  const defaultFilterValue = category.properties.map((p) => ({
    name: p.name,
    value: "all",
  }));

  const [products, setProducts] = useState(defaultProduct);
  const [filterValue, setFilterValue] = useState(defaultFilterValue);
  const [sort, setSort] = useState(defaultSorting);
  const [isLoading, setIsLoading] = useState(false);
  const [filtersChanged, setFiltersChanged] = useState(false);

  function handleFilterChange(filterName, filterValue) {
    setFilterValue((prev) => {
      return prev.map((p) => ({
        name: p.name,
        value: p.name === filterName ? filterValue : p.value,
      }));
    });
    setFiltersChanged(true);
  }

  useEffect(() => {
    if (!filtersChanged) {
      return;
    }
    setIsLoading(true);
    const catIds = [category._id, ...(subCategories.map((c) => c._id) || [])];
    const params = new URLSearchParams();
    params.set("categories", catIds.join(","));
    params.set("sort", sort);
    filterValue.forEach((f) => {
      if (f.value !== "all") {
        params.set(f.name, f.value);
      }
    });
    const URL = `/api/products?${params.toString()}`;
    axios.get(URL).then((res) => {
      setProducts(res.data);
      setIsLoading(false);
    });
  }, [filterValue, sort, filtersChanged]);

  return (
    <>
      <Center>
        <CatHeader>
          <h1>{category.name}</h1>
          <FilterWrap>
            {category.properties.map((prop) => (
              <Filter>
                {prop.name}:
                <select
                  onChange={(ev) => {
                    handleFilterChange(prop.name, ev.target.value);
                  }}
                  value={filterValue.find((f) => f.name === prop.name).value}>
                  <option value={"all"}>All</option>
                  {prop.values.map((val) => (
                    <option key={val._id} value={val}>
                      {val}
                    </option>
                  ))}
                </select>
              </Filter>
            ))}
            <Filter>
              <span>Sorting:</span>
              <select
                value={sort}
                onChange={(e) => {
                  setSort(e.target.value);
                  setFiltersChanged(true);
                }}>
                <option value="price-asc">price, lowest first</option>
                <option value="price-desc">price, highest first</option>
                <option value="_id-desc">newest first</option>
                <option value="_id-asc">oldest first</option>
              </select>
            </Filter>
          </FilterWrap>
        </CatHeader>
        {isLoading ? (
          <Loader fullWidth />
        ) : (
          <>
            {products.length > 0 ? (
              <ProductsGrid products={products} />
            ) : (
              <p>Sorry, no products found</p>
            )}
          </>
        )}
      </Center>
    </>
  );
}

export async function getServerSideProps(context) {
  const category = await Category.findById(context.query.id);
  const subCategories = await Category.find({ parent: category._id });
  const catIds = [category._id, ...subCategories.map((c) => c._id)];
  const products = await Product.find({ category: catIds });
  return {
    props: {
      category: JSON.parse(JSON.stringify(category)),
      subCategories: JSON.parse(JSON.stringify(subCategories)),
      products: JSON.parse(JSON.stringify(products)),
    },
  };
}
