import { createGlobalStyle } from "styled-components";
import CartContextProvider from "@/components/context/Cart";
import Header from "@/components/Nav/Navbar";

const GlobalStyle = createGlobalStyle`
  @import url('https://fonts.googleapis.com/css2?family=Nunito+Sans&display=swap');
  body{
  padding: 0;
  margin:0;
  font-family: 'Nunito Sans', sans-serif;
}`;

export default function App({ Component, pageProps }) {
  return (
    <>
      <GlobalStyle />
      <CartContextProvider>
        <Header />
        <Component {...pageProps} />
      </CartContextProvider>
    </>
  );
}
