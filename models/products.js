import mongoose from "mongoose";

const { Schema, model, models } = mongoose;

const productSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    category:{
        type: mongoose.Types.ObjectId, ref:'Category'
    },
    description: String,
    price: {
        type: Number,
        required: true
    },
    images: {
        type: [String]
    },
    properties: {type: Object}
}, {
    timestamps: true
});

export const Product = models?.Product || model('Product', productSchema);
