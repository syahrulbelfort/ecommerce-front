import React, { useContext, useState } from "react";
import Link from "next/link";
import styled from "styled-components";
import Center from "../Center";
import { CartContext } from "../context/Cart";
import { RxHamburgerMenu } from "react-icons/rx";
import { AiOutlineCloseCircle } from "react-icons/ai";

const StyledHeader = styled.header`
  background-color: white;
`;

const Logo = styled(Link)`
  color: black;
  text-decoration: none;
`;

const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 20px 0;
`;

const NavLink = styled(Link)`
  display: block;
  color: black;
  text-decoration: none;
  padding: 10px 0;
  @media screen and (min-width: 768px) {
    padding: 0;
  }
`;

const StyledNav = styled.nav`
  ${(props) =>
    props.mobilenavactive
      ? `
    display: block;
  `
      : `
    display: none;
  `}
  gap: 15px;
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  padding: 70px 20px 20px;
  background-color: white;
  @media screen and (min-width: 768px) {
    display: flex;
    position: static;
    padding: 0;
  }
`;

const NavImg = styled.img`
  width: 50px;
`;

const NavButton = styled.button`
  background-color: transparent;
  width: 30px;
  height: 30px;
  border: 0;
  color: black;
  cursor: pointer;
  position: relative;
  z-index: 3;
  @media screen and (min-width: 768px) {
    display: none;
  }
`;

export default function Header() {
  const { cartProducts } = useContext(CartContext);
  const [mobileNavActive, setMobileNavActive] = useState(false);

  return (
    <StyledHeader>
      <Center>
        <Wrapper>
          <Logo href={"/"}>
            <NavImg src="https://www.pngmart.com/files/4/Nike-Logo-PNG-Pic.png" />
          </Logo>
          <StyledNav mobilenavactive={mobileNavActive}>
            <NavLink href={"/"}>Home</NavLink>
            <NavLink href={"/allproducts"}>All products</NavLink>
            <NavLink href={"/categories"}>Categories</NavLink>
            <NavLink href={"/bag"}>Bag ({cartProducts.length})</NavLink>
          </StyledNav>
          <NavButton onClick={() => setMobileNavActive((prev) => !prev)}>
            {mobileNavActive ? (
              <AiOutlineCloseCircle size={`2em`} />
            ) : (
              <RxHamburgerMenu size={`2em`} />
            )}
          </NavButton>
        </Wrapper>
      </Center>
    </StyledHeader>
  );
}
