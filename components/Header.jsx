import React, { useContext } from 'react';
import Link from 'next/link';
import styled from 'styled-components';
import Center from './Center';
import {CartContext} from './context/Cart';


  const StyledHeader = styled.header`
    background-color: white;
  `;

  const Logo = styled(Link)`
  color:black;
  text-decoration:none;
  `;

  const Wrapper = styled.div`
  display:flex;
  justify-content:space-between;
  padding:20px 0;
  `
  const NavLink = styled(Link)`
  color:black;
  text-decoration:none;
  `
  const StyledNav = styled.nav`
    display:flex;
    gap: 10px;
  `

  const NavImg = styled.img`
    width: 50px;
  `
export default function Header() {

  const { cartProducts } = useContext(CartContext)

  return (
    <StyledHeader>
    <Center>
        <Wrapper>
      <Logo href={'/'}><NavImg src='https://www.pngmart.com/files/4/Nike-Logo-PNG-Pic.png'/></Logo>
      <StyledNav>
        <NavLink href={'/'}>Home</NavLink>
        <NavLink href={'/products'}>All products</NavLink>
        <NavLink href={'/categories'}>Categories</NavLink>
        <NavLink href={'/bag'}>Bag ({cartProducts.length})</NavLink>
      </StyledNav>
        </Wrapper>
    </Center>
    </StyledHeader>
  );
}
