import Center from "@/components/Center";
import PrimaryBtn from "@/components/PrimaryBtn";
import ProductImages from "@/components/ProductImages";
import WhiteBox from "@/components/WhiteBox";
import { CartContext } from "@/components/context/Cart";
import { MongooseConnect } from "@/libs/mongoose";
import { Product } from "@/models/products";
import React, { useContext } from "react";
import styled from "styled-components";
import { useRouter } from "next/router";

const ColumnWrap = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  @media screen and (min-width: 768px) {
    grid-template-columns: 1fr 1fr;
    gap: 100px;
  }
`;
const Top = styled.div`
  margin-top: 100px;
`;

export default function ProductPage({ product }) {
  const { addToBag } = useContext(CartContext);
  const router = useRouter();
  const { pathname } = router;

  return (
    <>
      <Top>
        <Center>
          <h2>{product?.title}</h2>
          {pathname.startsWith("/product/") && (
            <>
              <a href="/allproducts">All products {">"}</a>
              <a href={`/product/${product?._id}`}>{product?.title}</a>
            </>
          )}
          <ColumnWrap>
            <WhiteBox>
              <ProductImages images={product.images} />
            </WhiteBox>
            <div>
              <h2>{product.title}</h2>
              <h1>Rp. {product.price.toLocaleString()}</h1>
              <p>{product.description}</p>
              <PrimaryBtn onClick={() => addToBag(product._id)}>
                Add to bag
              </PrimaryBtn>
            </div>
          </ColumnWrap>
        </Center>
      </Top>
    </>
  );
}

export async function getServerSideProps(context) {
  await MongooseConnect();
  const { id } = context.query;
  const product = await Product.findById(id);
  return {
    props: {
      product: JSON.parse(JSON.stringify(product)),
    },
  };
}
